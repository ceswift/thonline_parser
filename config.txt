####################################################################################################
# This file contains the reg_ex pairs to search to/from on the webpage.
# File is basic where the first uncommented line is treated as a start, and the next as an end
# '#' are comment lines
# Example:
# '<div example_start>'
# '<div example_end>'
####################################################################################################
# Pair 1
# Pair 1: Start
<div  itemprop="articleBody" class="asset-content  subscriber-premium">
# Pair 1: End
<div id="tncms-region-article_bottom_content" class="tncms-region hidden-print"></div>
#
# Pair 2
# Pair 2: Start
<div id="article-body"  itemprop="articleBody" class="asset-content  subscriber-premium">
# Pair 2: End
<div id="tncms-region-article_bottom_content" class="tncms-region hidden-print"></div>
#
# EoF