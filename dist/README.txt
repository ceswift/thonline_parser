Update the start/end pairs in the CONFIG.TXT file 
  (various start/end pairs may need to be searched through, a few are provided in the file
The regular expression will return all text between start & end 
  (ie: start * end)
The returned RegExp is then modified to remove all the styles that hide text
The final output is then saved as an HTML and opened