﻿import sys
import os
import re
import tkinter as tk
#import urllib2
from urllib.request import urlopen
import webbrowser

url = ""
entry = None
master = tk.Tk()

def decode_article(article):
    
    start = "<div class=\"subscriber-only encrypted-content\" style=\"display:none\">"
    end   = "</div>"
    find_text = start + "(.*)" + end
    m = re.split(find_text,article, re.DOTALL)
    
    #decode all odd numbered items
    for x in range(len(m)):
      if x % 2 == 1:
        data  = m[x]
        ndata = decode_region(str(data.encode("UTF-8", errors="backslashreplace")))
        m[x] = "" + ndata
    
    return "".join(m)

def decode_region(div_region):
  
  decoded_str = ""
  
  # Since the output in the html is scrambled, html converts some of the odd things like > to &gt;
  # prior to unscrambling, these need to be converted back to what they are
  IDX_FIND    = 0
  IDX_REPLACE = 1
  fnd_rpl_lst = [["&gt;",">"],["&lt;","<"],["\\xe2\\x80\\x99","V"],["\\xe2\\x80\\x9c","V"],["\\xe2\\x80\\x9d",""],["\\xc2\\xa0",""]]
  for fnd_rpl in fnd_rpl_lst:
    div_region = div_region.replace(fnd_rpl[IDX_FIND],fnd_rpl[IDX_REPLACE])
  
  for ch in div_region:
    int_value = ord(ch)
    if ( int_value >= 33 and int_value <= 126):
      decoded_chr  = chr(33 + ((int_value - 33 + 47) % 94))
      decoded_str += decoded_chr
    else:
      decoded_str += ch

  # Strip out HTML related normal data that got converted 
  decoded_str = decoded_str.replace("3V<p>","<p>")
  decoded_str = decoded_str.replace("</p>k^5:Gm","</p>")
  decoded_str = decoded_str.replace("4=2DDlQDF3D4C:36C\\@?=J 6?4CJAE65\\4@?E6?EQ DEJ=6lQ5:DA=2Ji?@?6Qm","")
  decoded_str = decoded_str.replace(":5lQE?4>D\\C68:@?\\2CE:4=60:?DE@CJ0>:55=6Q 4=2DDlQE?4>D\\C68:@? 9:556?\\AC:?EQmk^5:Gmk5:G 4=2DDlQDF3D4C:36C\\@?=J 6?4CJAE65\\4@?E6?EQ DEJ=6lQ5:DA=2Ji?@?6Qm","")
  decoded_str = decoded_str.replace(":5lQE?4>D\\C68:@?\\2CE:4=60:?DE@CJ03@EE@>Q 4=2DDlQE?4>D\\C68:@? 9:556?\\AC:?EQmk5:G :5lQE?4>D\\3=@4<\\`c_fcbcQ 4=2DDlQE?4>D\\3=@4<Qmk5:G 4=2DDl-VDa?!=2J6C\\5Ezz4F)3-V 52E2\\EJA6l-V7F==-Vmk^5:GmkD4C:AE EJA6l-VE6IE^;2G2D4C:AE-V DC4l-V^^6>365]D6?5E@?6HD]4@>^A=2J6Ca^6>3654@56]A9An7<l5Ezz4F)3U4:5l`bc_b-V 52E2\\EJA6l-VDa?$4C:AE-Vmk^D4C:AEmk^5:GmV","")
  decoded_str = decoded_str.replace("k5:G ", "")
  
  return decoded_str

def _get_reg_ex_search_pairs():
  """ Function parses the config.txt file and gets all the various search pairs start/end to parse between.
      Returns a List (empty if none) of all the pairs
  """
  pairs = []
  with open("config.txt") as file:
    next_is_start = True
    for line in file:
      if False == line.startswith('#'):
        # if we are on this line, then its one of the pairs (start/end)
        if next_is_start:
          start=line.rstrip()
          next_is_start=False
        else:
          pairs.append({"start":start,"end":line.rstrip()})
          next_is_start=True
  return pairs

def _entry_get():
  global url

  url = entry.get()
  master.destroy()

def _quit_pressed():
  master.destroy()  

if __name__ == '__main__':
  
  if (1 != (len(sys.argv) - 1)):
    
    master.title("TH Online page parser")
    tk.Label(master, text="Enter URL:").grid(row=0)
    entry = tk.Entry(master)
    entry.config(width=100)
    entry.grid(row=0, column=1)
    
    
    tk.Button(master, text='Quit', command=_quit_pressed).grid(row=2, column=0, sticky=tk.W, pady=4)
    tk.Button(master, text='Parse', command=_entry_get).grid(row=2, column=1, sticky=tk.W, pady=4)
    master.protocol("WM_DELETE_WINDOW", _quit_pressed)
    
    master.mainloop()
    
    if url == "":
      quit()
      
    response = urlopen(url)
    
    #input("Press any key to end...")
    text = response.read().decode('utf-8')
    
    #with open('th_output.html', 'w') as file:
    #  file.write(text)
  else:
    fn = sys.argv[1]
    #fn = os.path.basename(fn)
    os.chdir(os.path.dirname(os.path.realpath(__file__)))
  
    f = open(fn, "rt")
    text = f.read()
    f.close()

  # Use Regular expressions to search for the content
  # the content could be between various start/end keywords as noted in the 
  # config.txt file.  Get the pairs to search between
  search_pairs = _get_reg_ex_search_pairs()
  m = None
  for pair in search_pairs:
    start = pair['start']
    end = pair['end']
    find_text = start + "(.*)" + end
    m = re.search(find_text,text, re.DOTALL) 

    if m is not None:
      break
  
  if m is None:
    print("Parse failed...")
    #input("Press any key to end...")
    quit()
  else:
    #decode the middle of the article
    article = decode_article(m.group(0))
    
    # Replace all the display:none styles with blocks
    find_text = 'style="display:none"'
    repl_text = 'style="display:block"'
    m2 = re.sub(find_text,repl_text,article)
    
    with open('th_output.html', 'w') as file:
      file.write(m2)
    
    webbrowser.open('file://' + os.path.realpath('th_output.html'))
